// Constants
const postForm = document.querySelector('#form-add-post'),
	postEntries = document.querySelector('#div-post-entries'),
	txtTitle = document.querySelector('#txt-title'),
	txtBody = document.querySelector('#txt-body'),
	editId = document.querySelector('#txt-edit-id'),
	editTitle = document.querySelector('#txt-edit-title'),
	editBody = document.querySelector('#txt-edit-body'),
	editForm = document.querySelector('#form-edit-post'),
	updateBtn = document.querySelector('#btn-submit-update');

// Event Listeners
postForm.addEventListener('submit', addPost);
editForm.addEventListener('submit', updatePost);

// Get POST
fetch('https://jsonplaceholder.typicode.com/posts').then(
(response) => 
	response.json()
).then((data) => {
	console.log(data)
	showPosts(data)
});

// Add POST
function addPost(event) {
	event.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: txtTitle.value,
			body: txtBody.value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json' // Indicates that the request body is in a json format
		}
	})
	.then((response) => response.json())
	.then((data) => {
		alert('Post successfully added')
	});

	txtTitle.value = null;
	txtBody.value = null;

}

function showPosts(posts) {
	let postEntry = '';

	posts.forEach((post) => {
		postEntry += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>

		</div>`
	})

	postEntries.innerHTML = postEntry;
}

function editPost(id) {
	let title =  document.querySelector(`#post-title-${id}`).innerHTML;
	let body =  document.querySelector(`#post-body-${id}`).innerHTML;

	editId.value = id;
	editTitle.value = title;
	editBody.value = body;
	updateBtn.removeAttribute('disabled');
}

function updatePost(event) {
	event.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: editId.value,
			title: editTitle.value,
			body: editBody.value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		alert('Post Edited successfully');

		editId.value = null;
		editTitle.value = null;
		editBody.value = null;
		updateBtn.setAttribute('disabled', true);
	})
}

function deletePost(id) {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE',
	});

	alert(`Post ${id} successfully deleted`);
	document.querySelector(`#post-${id}`).remove();
}